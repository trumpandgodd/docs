## How to handle backport requests?

Our [maintaince policy] limits the patch releases only to the previous stable version, however, there
may be circumstances in which a self-managed customer requires to patch previous versions. When this happens,
a GitLab team member opens a backport request and starts the [backport request process]. Backporting to previous
versions is a time-consuming and a manual task performed by Release Managers, because of it,
Release Managers should only accept these requests on exceptional cases.

The purpose of this guide is offering guidance to Release Managers when dealing with backport requests.

In general, when dealing with backport requests, Release Managers need to:

* Review the requirements.
* Assess the backport request.
* Schedule the backport request.

### Review the requirements

When assigned to a backport request, Release Managers should ensure the [backport request process]
requirements are met, for that:

* Review the issue and merge request have the appropriate labels. At the very minimum:
  * They should be bugs not features, backport requests are limited to regressions only.
  * They should have priority and severity labels.
* Ensure the impact of the bug fix is thoroughly explained and enough evidence about the necessity
  of the backport is provided. Evidence can be GitLab issues or comments, Zendesk tickets, etc.
* Ensure it explains how many strategic self-hosted customers are being affected by the regression.
* Ensure the bug fix is a critical one.

If any of these fails, the backport request should be rejected with an explanation of the reasoning.

### Assessing the backporting request

If the requirements have been met, Release Managers should ensure backporting to older versions
is required:

* Consider the impact the patch release may have on other users: For example, with one or more
  strategic customers affected, the request is more likely to have a high impact. Some questions to determine this:
  * How many strategic customers are affected by this?
  * What kind of customers are affected by this?
  * Does the bug incur any data-loss or security breaches?
* Ensure there's a plan for the self-hosted customers to upgrade. Self-hosted customers should be
  encouraged to upgrade to recent versions, those not only include features but security fixes
  as well. Some questions to determine this:
  * Is the customer unable to upgrade to a recent version?
  * Instead of backporting, could an option be for the customer to upgrade to a recent version?
  * What is the plan for the customer to upgrade to recent versions?
* Evaluate the workaround: Having an acceptable workaround probably involves less work than doing a patch
  release, particularly if the customer plans to upgrade soon.
* Analyze the release targeted for backporting. Backporting a merge request to two previous monthly releases
  is acceptable, backporting a merge request to five previous monthly releases is inconvenient and may
  lead to additional workload.

### Schedule the backport request

By this point, Release Managers should be certain there's a necessity for backporting a bug fix
to previous stable versions. The last step is to schedule the backport request, for this, it's important to
consider the monthly release schedule. If Release Managers are on the run-up to the monthly release,
that is in the last week of the release, it's advised to perform the backport requests after the 22nd.

[maintaince policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[backport request process]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
