# Merge-train

This doc refers to the https://ops.gitlab.net/gitlab-org/merge-train project, not the merge train feature
in the GitLab product.

Every project that can be part of a security release has a security mirror (usually under
gitlab.com/gitlab-org/security) that is not public. The canonical project and its security mirror are kept in sync
by using the mirroring feature of the GitLab product. The mirroring feature pushes commits from the
canonical repository to the security mirror.

During a security release, security fixes are merged into the security mirror to keep security vulnerability
information private until the security release is published. Once security merge requests have been merged,
Canonical and Security default branches will diverge, which breaks the automatic mirroring.

With mirroring broken during a security release, and since we deploy from the security mirror,
we need some way to push canonical commits to the security mirror in order to allow everyone's work
to continue being deployed to gitlab.com. This is where the merge-train project becomes useful.

A [scheduled
task](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/121/edit)
in release-tools will check for this divergence periodically and automatically
enable the [security merge-train pipeline schedule] to regularly update the
Security default branch until the divergence is resolved.

The [security merge-train pipeline schedule] will deal with this divergence
by updating Security default branch with the Canonical default branch content.

If you need to force an execution of the merge-train outside of this schedule,
you can:

* Go to the [security merge-train pipeline schedule].
* Click play.

This will trigger a merge-train that will merge Canonical default branch into Security
default branch. Full execution of merge-train takes approximately 6 minutes.

There is a [security merge-train pipeline schedule] for the Omnibus, GitLab and Gitaly projects.

The SSH key used by the merge-train project to clone the security repository is stored in 1password under
the Release vault with the name `merge-train: GitLab EE security/master deploy key`.

The SSH key is present in each project as a [Deploy Key](https://docs.gitlab.com/ee/user/project/deploy_keys/)
with the name `GitLab EE security/master sync`.

## Troubleshoot guide

### What to do if merge-train fails to update Security default branch?

There's a chance of a file being modified both on Canonical and on Security,
if this happens merge-train will fail when performing the merge between the two repositories.

The failure will generate a notification on the `#g_delivery` Slack channel.
Solving this will require coordination by the Release Managers:

* Analyze how difficult it is to recover from breakage.
  * If the conflict is easy to solve, fix it and push the changes to a branch with a merge request, ask another team member to review and merge once the pipeline is green.
  * If the conflict is more involved, ping the developers related to the changes, if they're not available, use the dev-on-call process.
* If the conflict blocks deployments, follow the [deployment blockers process] and raise an incident.

[security merge-train pipeline schedule]: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
[deployment blockers process]: https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers
